    $(document).ready(function() {
    $(".navbar-toggler").click(function() {
        $(".navbar-toggler").toggleClass(function () {
            return $(this).is('.navbar-toggler-on, .navbar-toggler-off') ? 'navbar-toggler-on navbar-toggler-off' : 'navbar-toggler-on';
        });
        $(".navbar-dropdown-menu").toggleClass(function () {
            return $(this).is('.show-menu, .close-menu') ? 'show-menu close-menu' : 'show-menu';
        });
        if($(".navbar-dropdown-menu").hasClass("show-menu")){
            $(".navbar-dropdown-menu").css("display" ,"block");
        }else{
            $(".navbar-dropdown-menu").css("display" ,"none");
        }
    });
    });